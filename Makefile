TicTacToe: main.o cli.o tictactoe.o grid.o
	g++ -o TicTacToe main.o cli.o tictactoe.o grid.o

main.o :
	g++ -std=c++11 -c main.cpp -o main.o

cli.o :
	g++ -std=c++11 -c cli.cpp -o cli.o

tictactoe.o :
	g++ -std=c++11 -c tictactoe.cpp -o tictactoe.o

grid.o :
	g++ -std=c++11 -c grid.cpp -o grid.o

clean :
	rm *.o
	rm TicTacToe
