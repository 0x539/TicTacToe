/*
Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This file is part of TicTacToe

TicTacToe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TicTacToe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TicTacToe.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "cli.h"

Cli::Cli() {}

void Cli::afficherGrille() {
  std::cout << std::string(50, '\n');
  std::string ligneTirets = "-------------";
  std::string ligne1, ligne2, ligne3;

  ligne1 = "| " + this->nbtocr(m_game.getSquare(0), 0) +
    " | " + this->nbtocr(m_game.getSquare(1), 1) +
    " | " + this->nbtocr(m_game.getSquare(2), 2) + " |";

  ligne2 = "| " + this->nbtocr(m_game.getSquare(3), 3) +
    " | " + this->nbtocr(m_game.getSquare(4), 4) +
    " | " + this->nbtocr(m_game.getSquare(5), 5) + " |";

  ligne3 = "| " + this->nbtocr(m_game.getSquare(6), 6) +
    " | " + this->nbtocr(m_game.getSquare(7), 7) +
    " | " + this->nbtocr(m_game.getSquare(8), 8) + " |";


  std::cout << ligneTirets << std::endl
            << ligne1 << std::endl
            << ligneTirets << std::endl
            << ligne2 << std::endl
            << ligneTirets << std::endl
            << ligne3 << std::endl
            << ligneTirets << std::endl;
}

void Cli::choix() {
    if(m_game.won()==0) {
        if(m_game.aqlt()==1)
            std::cout << "Au croix de jouer..." << std::endl;
        else
            std::cout << "Au ronds de jouer..." << std::endl;

        int choix;
        std::cout << "\nEntrer le numéro d'une case de 1 à 9: ";
        if (!(std::cin >> choix)) {
            std::cin.clear();
            std::cin.ignore(10000,'\n');
        }
        choix = this->parseChoice(choix);
	m_game.setCase(choix);
    }
}

void Cli::run() {
    while(m_game.won()==0){
        this->afficherGrille();
        this->choix();
    }

   this->afficherGrille();
    if(m_game.won()==1)
      std::cout << "Les croix ont gagné!" << std::endl;
    else if(m_game.won()==2)
      std::cout << "Les ronds ont gagné!" << std::endl;
    else
      std::cout << "Match Nul!" << std::endl;
}

std::string Cli::nbtocr(int nb, int square) {
    if(nb==0)
      return std::to_string(this->nbSquare(square));
    else if(nb==1)
        return "\033[0;31mX\033[0m";
    else
        return "\033[0;32mO\033[0m";
}

int Cli::parseChoice(int choice) {
	switch(choice) {
	case 1:
		return 6;
		break;
	case 2:
		return 7;
		break;
	case 3:
		return 8;
		break;
	case 4:
		return 3;
		break;
	case 5:
		return 4;
		break;
	case 6:
		return 5;
		break;
	case 7:
		return 0;
		break;
	case 8:
		return 1;
		break;
	case 9:
		return 2;
		break;
	}
}

int Cli::nbSquare(int square) {
	switch(square) {
	case 6:
		return 1;
		break;
	case 7:
		return 2;
		break;
	case 8:
		return 3;
		break;
	case 3:
		return 4;
		break;
	case 4:
		return 5;
		break;
	case 5:
		return 6;
		break;
	case 0:
		return 7;
		break;
	case 1:
		return 8;
		break;
	case 2:
		return 9;
		break;
	}
}