/*
Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This file is part of TicTacToe

TicTacToe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TicTacToe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "grid.h"

Grid::Grid() {
  for(int i(0); i<9; i++)
    m_grid[i]=0;
}

int Grid::getSquare(int num) {return m_grid[num];}

int Grid::setCase(int nb, int val) {
  if (m_grid[nb] == 0) {
    if(val>=0 && val<=2)
      m_grid[nb] = val;
    else
      return -2;
  }
  else
    return -1;
  return 0;
}
