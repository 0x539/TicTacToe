/*
Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This file is part of TicTacToe

TicTacToe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TicTacToe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

class Grid {
 public:
  Grid();
  int getSquare(int num);
  int setCase(int nb, int val);

 private:
  int m_grid[9];
};
