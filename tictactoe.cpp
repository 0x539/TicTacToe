/*
Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This file is part of TicTacToe

TicTacToe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TicTacToe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tictactoe.h"

Tictactoe::Tictactoe() : m_won(false), m_coup(0), m_aqlt(1){}

int Tictactoe::setCase(int nb) {
  if(m_won!=0)
    return -3;
  else if(m_coup==9) {
    m_won=-1;
    return -4;
  }
  int retour = m_grid.setCase(nb,m_aqlt);
  if(retour==0) {
    checkWin();
    ((m_aqlt==1)? m_aqlt=2 : m_aqlt=1);
    m_coup++;
  }
  return retour;
}

int Tictactoe::won() {
  if(m_coup>=9)
    m_won=-1;
  return m_won;
}

void Tictactoe::checkWin() {
  this->checkColumns();
  this->checkRows();
  this->checkDiagonals();
}

void Tictactoe::checkColumns() {
  this->compterCR(0, 7, 3);
  this->compterCR(1, 8, 3);
  this->compterCR(2, 9, 3);
}

void Tictactoe::checkRows() {
  this->compterCR(0, 3, 1);
  this->compterCR(3, 6, 1);
  this->compterCR(6, 9, 1);
}

void Tictactoe::checkDiagonals() {
  this->compterCR(0, 9, 4);
  this->compterCR(2, 7, 2);
}

void Tictactoe::compterCR(int nbDebut, int nbFin, int pas) {
  int nbCroix(0), nbRond(0);

  for (int i(nbDebut); i < nbFin; i+=pas) {
    if(m_grid.getSquare(i)==1)
      nbCroix++;
    else if(m_grid.getSquare(i)==2)
      nbRond++;
  }
  if(nbCroix==3)
    m_won=1;
  else if(nbRond==3)
    m_won=2;
}

int Tictactoe::getSquare(int nb) {return m_grid.getSquare(nb);}

int Tictactoe::aqlt() {return m_aqlt;}
