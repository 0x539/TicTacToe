/*
Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This file is part of TicTacToe

TicTacToe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TicTacToe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <random>
#include "grid.h"

class Tictactoe {
 public:
  Tictactoe();
  //-1 -> case déjà remplie; -2 -> valeur incorrecte et -3 -> partie terminée
  int setCase(int nb);
  int won();
  int getSquare(int nb);
  //A qui le tour ?
  int aqlt();

 private:
  void checkWin();
  void checkColumns();
  void checkRows();
  void checkDiagonals();
  //compter Croix Rond nbDebut commence a 0 et nbFin commence a 1
  void compterCR(int nbDebut, int nbFin, int pas);
  int m_won, m_coup, m_aqlt;
  Grid m_grid;
};
